package fr.mds.qualitelogicieltp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QualitelogicieltpApplication {

	public static void main(String[] args) {
		SpringApplication.run(QualitelogicieltpApplication.class, args);
	}

}
