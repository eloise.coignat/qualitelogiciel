package fr.mds.qualitelogicieltp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.mds.qualitelogicieltp.entities.Product;
import fr.mds.qualitelogicieltp.repositories.ProductRepository;
import fr.mds.qualitelogicieltp.services.ProductService;

@Controller
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    private ProductService myService;

    public ProductController(ProductService productService) {
        this.myService = productService;
    }
    
    @RequestMapping(value = { "/index", "/" })
    public String index(Model myModel) {
    	myModel.addAttribute("page", "Product index");
    	myModel.addAttribute("items", myService.findAll());
        return "product/index";
    }

    @GetMapping(value = {"/create"})
    public String createGet(Model myModel) {
    	myModel.addAttribute("page", "Product create");
        return "product/create";
    }

    @PostMapping(value = {"/create"})
    public String createPost(@ModelAttribute Product product) {
        if (product != null) {
            myService.save(product);
        }
        return "redirect:index";
    }

    @PostMapping(value = {"/delete"})
    public String delete(Long id) {
    	Product product = myService.getProductById(id);
        myService.delete(product);
        return "redirect:index";
    }

    @GetMapping(value = {"/show/{id}"})
    public String details(Model myModel, @PathVariable(value = "id") String id) {
    	myModel.addAttribute("product", myService.getProductById(Long.parseLong(id)));
        return "product/details";
    }

}
