package fr.mds.qualitelogicieltp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.mds.qualitelogicieltp.entities.User;
import fr.mds.qualitelogicieltp.repositories.ProductRepository;
import fr.mds.qualitelogicieltp.repositories.UserRepository;
import fr.mds.qualitelogicieltp.services.ProductService;
import fr.mds.qualitelogicieltp.services.UserService;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService myService;
    private ProductService myProdService;

    public UserController(UserService userService, ProductService productService) {
        this.myService = userService;
        this.myProdService = productService;
    }
    
    @RequestMapping(value = { "/index", "/" })
    public String index(Model myModel) {
    	myModel.addAttribute("page", "User index");
    	myModel.addAttribute("items", myService.findAll());
        return "user/index";
    }

    @GetMapping(value = {"/create"})
    public String createGet(Model myModel) {
    	myModel.addAttribute("page", "User create");
    	myModel.addAttribute("products", myProdService.findAll());
        return "user/create";
    }

    @PostMapping(value = {"/create"})
    public String createPost(@ModelAttribute User user) {
        if (user != null) {
            myService.save(user);
        }
        return "redirect:index";
    }

    @PostMapping(value = {"/delete"})
    public String delete(Long id) {
    	User user = myService.getUserById(id);
        myService.delete(user);
        return "redirect:index";

    }

    @GetMapping(value = {"/show/{id}"})
    public String details(Model myModel, @PathVariable(value = "id") String id) {
    	myModel.addAttribute("user", myService.getUserById(Long.parseLong(id)));
    	myModel.addAttribute("items", myService.getUserById(Long.parseLong(id)).getProducts());
        return "user/details";
    }

}

