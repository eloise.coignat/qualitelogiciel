package fr.mds.qualitelogicieltp.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Eloise
 *
 */
@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String firstname;
	private String lastname;
	
	@OneToMany
	@JoinColumn(name = "MY_FK_COL", nullable = true)
	private List<Product> products = new ArrayList<Product>();
	
	public User() {
	    super();
	}
	
	public User(Long id, String firstname, String lastname) {
	    super();
	    this.id = id;
	    this.firstname = firstname;
	    this.lastname = lastname;
	}

	public User(Long id, String firstname, String lastname, List<Product> products) {
	    super();
	    this.id = id;
	    this.firstname = firstname;
	    this.lastname = lastname;
	    this.products = products;
	}
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Id : " + id + "\nFirstname : " + firstname + "\nLastname : " + lastname;
	}
	
}
