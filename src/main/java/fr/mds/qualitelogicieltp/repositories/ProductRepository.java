package fr.mds.qualitelogicieltp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.mds.qualitelogicieltp.entities.*;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
	 public Product getProductById(Long productId);
}
