package fr.mds.qualitelogicieltp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.mds.qualitelogicieltp.entities.*;

public interface UserRepository extends JpaRepository<User, Long> {
	
	public User getUserById(Long userId);
}
