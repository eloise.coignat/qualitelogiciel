package fr.mds.qualitelogicieltp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.mds.qualitelogicieltp.entities.Product;
import fr.mds.qualitelogicieltp.repositories.ProductRepository;

@Service
public class ProductService {

    @Autowired
    private ProductRepository myProductRepository;

    public Product save(Product item) {
        return this.myProductRepository.save(item);
    }

    public void delete(Product item) {
        this.myProductRepository.delete(item);
    }

    public List<Product> findAll() {
        return this.myProductRepository.findAll();
    }

    public Product getProductById(Long id) {
        return this.myProductRepository.getProductById(id);
    }

    public void saveAll(List<Product> products) {
        for (Product product : products) {
            product.setId(myProductRepository.save(product).getId());
        }
    }

}

