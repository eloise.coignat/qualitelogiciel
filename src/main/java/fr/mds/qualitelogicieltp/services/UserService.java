package fr.mds.qualitelogicieltp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.mds.qualitelogicieltp.entities.User;
import fr.mds.qualitelogicieltp.repositories.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository myUserRepository;

    public User save(User item) {
        return this.myUserRepository.save(item);
    }

    public void delete(User item) {
        this.myUserRepository.delete(item);
    }
    
    public List<User> findAll() {
        return this.myUserRepository.findAll();
    }

    public User getUserById(Long id) {
        return this.myUserRepository.getUserById(id);
    }

    public void saveAll(List<User> users) {
        for (User user : users) {
            user.setId(myUserRepository.save(user).getId());
        }
    }

}

