<#import "/spring.ftl" as spring/>

<head>
    <#include "../includable/bootstrap.ftl">
</head>
<style>
	body {
		margin-left:15px;
	}
</style>
<body>
    <H1>User details</H1>   
    <p>Id : ${user.id} </p>
    <p>Firstname : ${user.firstname}</p>
    <p>Lastname : ${user.lastname}</p>
    <a href="../index">Retour liste users</a><br><br>
    
     <p>Products : </p>
     <table class="table table-bordered table-hover">
        <tr>
        <th>Name</th>
        <th>Price</th>
        </tr>

        <#list items as item>
            <tr>
                <td>${item.name}</td>
                <td>${item.price}</td>
            </tr>
        </#list>
    </table>

</body>
