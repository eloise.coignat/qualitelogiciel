package fr.mds.qualitelogicieltp.mock.repositories;

import java.util.Arrays;
import java.util.Optional;

import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import fr.mds.qualitelogicieltp.entities.Product;
import fr.mds.qualitelogicieltp.repositories.ProductRepository;

public class MockProductRepository {
	
	protected final ProductRepository repository;
	
	public Long count;

	public Product entity;

	public Product resultEntity;

	public Optional<Product> resultOptional;

	public MockProductRepository (ProductRepository repository) {
	    this.repository = repository;
	    this.count = 1L;

	    this.entity = new Product();
	    this.entity.setName("Raclette");
	    this.entity.setPrice((float)45);
	}

	public void intialize() {
	    // this.configure();

	    this.resultEntity = new Product();
	    this.resultEntity.setId(this.entity.getId());
	    this.resultEntity.setName(this.entity.getName());
	    this.resultEntity.setPrice(this.entity.getPrice());

	    this.resultEntity.setId(1L);
	    this.resultOptional = Optional.of(this.resultEntity);

	    Mockito.when(this.repository.findById(ArgumentMatchers.any())).thenReturn(this.resultOptional);

	    Mockito.when(this.repository.findAll((Pageable) ArgumentMatchers.any()))
	        .thenReturn(new PageImpl<>(Arrays.asList(this.resultEntity)));
	    
	    Mockito.when(this.repository.count()).thenAnswer(new Answer<Long>() {
            @Override
            public Long answer(InvocationOnMock invocation) throws Throwable {
                return MockProductRepository.this.count;
            }
        });

	    Mockito.when(this.repository.save(ArgumentMatchers.any())).thenAnswer(new Answer<Product>() {

	      @Override
	      public Product answer(InvocationOnMock invocation) throws Throwable {
	        Product product = invocation.getArgument(0);
	        product.setId(1L);
	        MockProductRepository.this.count++;
	        return MockProductRepository.this.resultEntity;
	      }
	    });
	    
	    Mockito.doAnswer((i) -> {
	        MockProductRepository.this.count--;
	        return null;
	      }).when(this.repository).delete(ArgumentMatchers.any());
	}

}
