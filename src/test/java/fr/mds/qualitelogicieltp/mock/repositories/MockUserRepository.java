package fr.mds.qualitelogicieltp.mock.repositories;

import java.util.Arrays;
import java.util.Optional;

import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import fr.mds.qualitelogicieltp.entities.User;
import fr.mds.qualitelogicieltp.repositories.UserRepository;

public class MockUserRepository {
	
	 protected final UserRepository repository;
	 
	 public Long count;

	 public User entity;

	 public User resultEntity;

	 public Optional<User> resultOptional;

	 public MockUserRepository (UserRepository repository) {
	    this.repository = repository;
	    this.count = 1L;

	    this.entity = new User();
	    this.entity.setFirstname("firstname1");
	    this.entity.setLastname("lastname1");
	 }

	 public void intialize() {
	    // this.configure();

	    this.resultEntity = new User();
	    this.resultEntity.setId(this.entity.getId());
	    this.resultEntity.setFirstname(this.entity.getFirstname());
	    this.resultEntity.setLastname(this.entity.getLastname());

	    this.resultEntity.setId(1L);
	    this.resultOptional = Optional.of(this.resultEntity);

	    Mockito.when(this.repository.findById(ArgumentMatchers.any())).thenReturn(this.resultOptional);

	    Mockito.when(this.repository.findAll((Pageable) ArgumentMatchers.any()))
	        .thenReturn(new PageImpl<>(Arrays.asList(this.resultEntity)));	    
	    
	    Mockito.when(this.repository.count()).thenAnswer(new Answer<Long>() {
            @Override
            public Long answer(InvocationOnMock invocation) throws Throwable {
                return MockUserRepository.this.count;
            }
        });
	    
	    Mockito.when(this.repository.save(ArgumentMatchers.any())).thenAnswer(new Answer<User>() {

		      @Override
		      public User answer(InvocationOnMock invocation) throws Throwable {
		        User user = invocation.getArgument(0);
		        user.setId(1L);
		        MockUserRepository.this.count++;
		        return MockUserRepository.this.resultEntity;
		      }
		    });
	    
	    Mockito.doAnswer((i) -> {
	        MockUserRepository.this.count--;
	        return null;
	      }).when(this.repository).delete(ArgumentMatchers.any());
	     
	  }
	 

}
