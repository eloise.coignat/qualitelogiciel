package fr.mds.qualitelogicieltp.servicestest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.mds.qualitelogicieltp.QualitelogicieltpApplicationTests;
import fr.mds.qualitelogicieltp.entities.Product;
import fr.mds.qualitelogicieltp.entities.User;
import fr.mds.qualitelogicieltp.mock.repositories.MockProductRepository;
import fr.mds.qualitelogicieltp.mock.repositories.MockUserRepository;
import fr.mds.qualitelogicieltp.repositories.ProductRepository;
import fr.mds.qualitelogicieltp.repositories.UserRepository;
import fr.mds.qualitelogicieltp.services.ProductService;

@ActiveProfiles("test")
@TestPropertySource(locations = { "classpath:application-test.properties" })
@SpringBootTest(classes = QualitelogicieltpApplicationTests.class)
public class ProductServiceTest {

	@MockBean
	private UserRepository myUserRepository;
	
	@MockBean
	private ProductRepository myProductRepository;
	
	@Autowired
	private ProductService myProductService;
	
	private Product entity;
	
	@BeforeEach
	public void setUp() throws Exception {
	    final MockProductRepository mock = new MockProductRepository(this.myProductRepository);
	    mock.intialize();
	    this.entity = mock.entity;
	}
	
	/**
	 * Test que l�insertion d�un �l�ment ajoute bien un enregistrement dans la base de donn�es
	 */
	@Test
	public void TestSaveOneProduct() {
		Long countBefore = myProductRepository.count();
		myProductService.save(this.entity);
		Long countAfter = myProductRepository.count();
		
		assertEquals(countBefore + 1, countAfter);
	}
	
	/**
	 * Test que l�insertion d�un �l�ment n�a pas alt�r� les donn�es de l�objet sauvegard�
	 */
	@Test
	public void TestSaveOneProduct2() {
        Long id = myProductService.save(this.entity).getId();
        Product productSelected = myProductRepository.findById(id).get();
        
        assertTrue(comparerSimple(this.entity, productSelected));
	}
	
	/**
	 * Test que la mise � jour d�un �l�ment n�a pas alt�r� les donn�es de l�objet sauvegard�
	 */
	@Test
	public void TestSaveOneProduct3() {
		myProductService.save(this.entity);
        Product productSelected = myProductRepository.findById(this.entity.getId()).get();
        productSelected.setName("Roblochon");
        myProductService.save(productSelected);
        Product productUpdated = myProductRepository.findById(this.entity.getId()).get();
        
        assertTrue(comparerSimple(productSelected, productUpdated));
	}
	
	/**
	 * Test qu�un �l�ment est r�cup�r� avec les bonnes donn�es
	 */
	@Test
	public void TestGetOneProduct() {
		Product productName = myProductRepository.findById((long)1).get();

        assertEquals(productName, new Product(1L, "Raclette", 45F));		
	}
	
	/**
	 * Test qu�une liste est r�cup�r�e avec les bonnes donn�es
	 */
	@Test
	public void TestGetListProduct() {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product(null, "Tartes aux fraise", (float)8));
		products.add(new Product(null, "Lentilles", (float) 3));
		
		myProductService.saveAll(products);
		
		List<Product> productsSelected = myProductService.findAll();
		
		assertEquals(productsSelected.get(0).getName(), "Raclette");
		assertEquals(productsSelected.get(1).getPrice(),  (float)8);
		
	}
	
	/**
	 * Test que la suppression d�un �l�ment d�cr�mente le nombre d�enregistrement pr�sent
	 */
	@Test
	public void TestDeleteOneProduct() {
        myProductService.save(this.entity);
        Long countBefore = myProductRepository.count();
        myProductService.delete(this.entity);
        Long countAfter = myProductRepository.count();

        assertEquals(countBefore - 1, countAfter);
	}
	
	/**
	 * Test que la suppression d�un �l�ment supprime bien le bon �l�ment
	 */
	@Test
	public void TestDeleteOneProduct2() {		
        Long id = myProductRepository.save(this.entity).getId();
        myProductService.delete(this.entity);

        Product productDeleted = myProductService.getProductById(id);
        assertNull(productDeleted);		
	}
	
	public static boolean comparerSimple(Product product1, Product product2) {
		boolean result = true;

        if (!product1.getId().equals(product2.getId())) {
            result = false;
        }
        if (!product1.getName().equals(product2.getName())) {
            result = false;
        }
        if (!product1.getPrice().equals(product2.getPrice())) {
            result = false;
        }

        return result;
    }

}
