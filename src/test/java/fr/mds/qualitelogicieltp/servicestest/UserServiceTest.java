package fr.mds.qualitelogicieltp.servicestest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import fr.mds.qualitelogicieltp.QualitelogicieltpApplicationTests;
import fr.mds.qualitelogicieltp.entities.User;
import fr.mds.qualitelogicieltp.mock.repositories.MockUserRepository;
import fr.mds.qualitelogicieltp.repositories.ProductRepository;
import fr.mds.qualitelogicieltp.repositories.UserRepository;
import fr.mds.qualitelogicieltp.services.UserService;

@ActiveProfiles("test")
@TestPropertySource(locations = { "classpath:application-test.properties" })
@SpringBootTest(classes = QualitelogicieltpApplicationTests.class)
public class UserServiceTest {

	@MockBean
	private UserRepository myUserRepository;
	
	@MockBean
	private ProductRepository myProductRepository;
	
	@Autowired
	private UserService myUserService;
	
	private User entity;
	
	@BeforeEach
	public void setUp() throws Exception {
	    final MockUserRepository mock = new MockUserRepository(this.myUserRepository);
	    mock.intialize();
	    this.entity = mock.entity;
	}
	
	/**
	 * Test que l�insertion d�un �l�ment ajoute bien un enregistrement dans la base de donn�es
	 */
	@Test
	public void TestSaveOneUser() {
		Long countBefore = myUserRepository.count();
		myUserService.save(this.entity);
		Long countAfter = myUserRepository.count();
		
		assertEquals(countBefore + 1, countAfter);
	}
	
	/**
	 * Test que l�insertion d�un �l�ment n�a pas alt�r� les donn�es de l�objet sauvegard�
	 */
	@Test
	public void TestSaveOneUser2() {
        Long id = myUserService.save(this.entity).getId();
        User userSelected = myUserRepository.findById(id).get();
        
        assertTrue(comparerSimple(userSelected, this.entity));
	}
	
	/**
	 * Test que la mise � jour d�un �l�ment n�a pas alt�r� les donn�es de l�objet sauvegard�
	 */
	@Test
	public void TestSaveOneUser3() {
		myUserService.save(this.entity);
        User userSelected = myUserRepository.findById(this.entity.getId()).get();
        userSelected.setFirstname("Mimi");
        myUserService.save(userSelected);
        User userUpdated = myUserRepository.findById(this.entity.getId()).get();
        
        assertTrue(comparerSimple(userSelected, userUpdated));
	}
	
	/**
	 * Test qu�un �l�ment est r�cup�r� avec les bonnes donn�es
	 */
	@Test
	public void TestGetOneUser() {
		User user = myUserRepository.findById(this.entity.getId()).get();

        assertEquals(user, new User((long)1, "firstname1", "lastname1"));		
	}
	
	/**
	 * Test qu�une liste est r�cup�r�e avec les bonnes donn�es
	 */
	@Test
	public void TestGetListUser() {		
		List<User> users = new ArrayList<User>();
		users.add(new User(null, "Brandon", "Habasque"));
		users.add(new User(null, "Emilien", "Gantois"));
		users.add(new User(null, "sdifuz", "isgq"));
		
		myUserService.saveAll(users);
		
		assertEquals(myUserRepository.findAll().get(0).getFirstname(), "Elo�se");
	}
	
	/**
	 * Test que la suppression d�un �l�ment d�cr�mente le nombre d�enregistrement pr�sent
	 */
	@Test
	public void TestDeleteOneUser() {
        myUserService.save(this.entity);
        Long countBefore = myUserRepository.count();
        myUserService.delete(this.entity);
        Long countAfter = myUserRepository.count();

        assertEquals(countBefore - 1, countAfter);
	}
	
	/**
	 * Test que la suppression d�un �l�ment supprime bien le bon �l�ment
	 */
	@Test
	public void TestDeleteOneUser2() {		
        Long id = myUserRepository.save(this.entity).getId();
        myUserService.delete(this.entity);

        User userDeleted = myUserService.getUserById(id);
        assertNull(userDeleted);		
	}
	
	
	public static boolean comparerSimple(User user1, User user2) {
		boolean result = true;

        if (!user1.getId().equals(user2.getId())) {
            result = false;
        }
        if (!user1.getFirstname().equals(user2.getFirstname())) {
            result = false;
        }
        if (!user1.getLastname().equals(user2.getLastname())) {
            result = false;
        }

        return result;
    }

}
